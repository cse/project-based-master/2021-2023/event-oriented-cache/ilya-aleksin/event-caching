package restapi

import (
	loads "github.com/go-openapi/loads"
	"github.com/pim/pam/poum/restapi/operations"
	"net/http"
)

func getAPI() (*operations.ThefactoryAPI, error) {
	swaggerSpec, err := loads.Analyzed(SwaggerJSON, "")
	if err != nil {
		return nil, err
	}
	api := operations.NewThefactoryAPI(swaggerSpec)
	return api, nil
}

func GetAPIHandler() (http.Handler, error) {
	api, err := getAPI()
	if err != nil {
		return nil, err
	}
	h := configureAPI(api)
	err = api.Validate()
	if err != nil {
		return nil, err
	}
	return h, nil
}

func TestApiExample(t *testing.T) {
	handler, err := restapi.GetAPIHandler()
	if err != nil {
		t.Fatal("get api handler", err)
	}
	ts := httptest.NewServer(handler)
	defer ts.Close()
	res, err := http.Get(ts.URL + "/v2/invalidate")
	if res != "OK" {
		t.Errorf("wrond output from cache manager")
	}
}
