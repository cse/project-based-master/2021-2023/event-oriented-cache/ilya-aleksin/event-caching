package cache

import "context"

type Cache interface {
	Get(ctx context.Context, key interface{}) (interface{}, error)
	Set(ctx context.Context, key, value interface{}) error
}

type CacheRedis struct {
	Addr string
}

func (c *CacheRedis) Get(ctx context.Context, key interface{}) (interface{}, error) {
	return "cache response", nil
}

func (c *CacheRedis) Set(ctx context.Context, key interface{}) error {
	return nil
}
