package main

import (
	"cache-sidecar/cache"
	"fmt"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
)

type Config struct {
	TargetPort string
}

var (
	config    Config
	localhost = "127.0.0.1"
	cacheImpl = cache.CacheRedis{
		Addr: "localhost:9000",
	}
)

func ProxyHandler(proxy *httputil.ReverseProxy) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		if r.Method == http.MethodGet {
			resp, err := cacheImpl.Get(r.Context(), r.URL)
			if err != nil {
				fmt.Println("error occured")
				return
			}
			fmt.Println(resp)
			return
		}
		proxy.ServeHTTP(w, r)
	}
}

func NewProxy(targetHost string) (*httputil.ReverseProxy, error) {
	url, err := url.Parse(targetHost)
	if err != nil {
		return nil, err
	}
	return httputil.NewSingleHostReverseProxy(url), nil
}

func main() {
	config = Config{
		TargetPort: "3000",
	}
	targetAddr := fmt.Sprintf("%s:%s", localhost, config.TargetPort)
	proxy, err := NewProxy(targetAddr)
	if err != nil {
		log.Fatalf("Failed to create reverse proxy with target address %s", targetAddr)
	}

	mux := http.NewServeMux()
	mux.HandleFunc("/", ProxyHandler(proxy))

	err = http.ListenAndServe(":9000", mux)
	if err != nil {
		log.Fatalf("Failed to start http server on port 9000: %s", err.Error())
	}
}
